gfpoken (1-3) unstable; urgency=medium

  * Team upload.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Move gfpoken to salsa.debian.org.
  * Fix FTBFS with GCC 10.
    Thanks to Reiner Herrmann for the patch. (Closes: #957271)
  * Simplify debian/rules.
  * Drop obsolete menu file.

 -- Markus Koschany <apo@debian.org>  Fri, 19 Feb 2021 19:49:28 +0100

gfpoken (1-2) unstable; urgency=medium

  * Upgrade debian/compat, so new debhelper version is actually used.
  * Fix argument order in debian/rules to make debhelper 9 happy.

 -- Bas Wijnen <wijnen@debian.org>  Mon, 06 Oct 2014 11:56:44 -0400

gfpoken (1-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Bas Wijnen ]
  * New upstream release
  * Remove patches, which are integrated upstream
  * Fix when building with --as-needed from Ubuntu, integrated upstream
    (Closes: #631694)
  * Fix transparent background, which was broken, integrated upstream
  * Update standards version to 3.9.6 (no changes needed)
  * Use debhelper 9 to get hardening

 -- Bas Wijnen <wijnen@debian.org>  Sun, 05 Oct 2014 23:48:32 -0400

gfpoken (0.32-2) unstable; urgency=low

  * Team upload.
  * Upgrade to new debhelper override_* rules
  * Use the dpkg-source v3 format
  * Add patch to fix FTBFS with binutils-gold (Closes: #554543)
  * Bump Standards-Version, no changes needed
  * Drop filename for GPL-any from copyright info, not needed
  * Add patch to direct blender output to the right place (Closes: #441126)
  * Packaging whitespace and wrapping fixes

 -- Paul Wise <pabs@debian.org>  Tue, 22 Mar 2011 23:01:18 +0800

gfpoken (0.32-1) unstable; urgency=low

  [ Bas Wijnen ]
  * New upstream release.
  * Add bounds check for button press events.  (Closes: #514463)
  * Updated standards version to 3.8.0 (no changes needed).
  * Changed Vcs-Browser to point to gfpoken files.

  [ Peter De Wachter ]
  * Added watch file.

 -- Bas Wijnen <wijnen@debian.org>  Sat, 11 Apr 2009 11:20:07 +0200

gfpoken (0.31-2) unstable; urgency=low

  * Reflect upgrade to Gtk+ 2 in Build-Depends.  (Closes: #464346)
  * Set Vcs-* links to Alioth.

 -- Bas Wijnen <wijnen@debian.org>  Wed, 06 Feb 2008 21:27:48 +0100

gfpoken (0.31-1) unstable; urgency=low

  * New upstream release.
    - Upgrade to Gtk+ 2.  (Closes: #456125)
  * Add Vcs-* and Homepage fields to debian/control.
  * Make copyright file machine readable.
  * Add transparency to menu icon.
  * Update standards version to 3.7.3 (no changes needed).
  * Remove explicit mention of utf-8 encoding from desktop file.
  (Closes: #453486)

 -- Bas Wijnen <wijnen@debian.org>  Tue, 05 Feb 2008 23:20:03 +0100

gfpoken (0.30-5) unstable; urgency=low

  * Update mktiles to new gimp version requirements.  (Closes: #450433)

 -- Bas Wijnen <wijnen@debian.org>  Sun, 11 Nov 2007 14:53:33 +0100

gfpoken (0.30-4) unstable; urgency=low

  * Add sharutils to Build-Depends for uudecode.  (Closes: #442231)

 -- Bas Wijnen <wijnen@debian.org>  Sun, 16 Sep 2007 20:49:04 +0200

gfpoken (0.30-3) unstable; urgency=low

  * Use fallback if blender fails to generate marbles.  This is a workaround
    for bug #441126.  It can be closed when blender bug #441216 is fixed.
  * Remove comment from Gimp script, to work around Gimp bug.
    (Closes: #441210)
  * Update desktop file to include categories.

 -- Bas Wijnen <wijnen@debian.org>  Fri, 07 Sep 2007 18:50:20 +0200

gfpoken (0.30-2) unstable; urgency=low

  * Don't let Gimp use real home directory.  (Closes: #440671)

 -- Bas Wijnen <wijnen@debian.org>  Thu, 06 Sep 2007 13:08:42 +0200

gfpoken (0.30-1) unstable; urgency=low

  * New upstream release.  (Closes: #439492)
  * License changed to GPL version 3 or later.
  * Automake dependency upgraded to 1.10.

 -- Bas Wijnen <wijnen@debian.org>  Wed, 29 Aug 2007 23:21:59 +0200

gfpoken (0.29-1) unstable; urgency=low

  * New upstream release.
  * Fix buttons which were not shown anymore. (Closes: #367495)
  * Add watch file.

 -- Bas Wijnen <shevek@fmf.nl>  Tue, 20 Jun 2006 11:40:20 +0200

gfpoken (0.28-1) unstable; urgency=low

  * New upstream release.
  * Add cross-compiling support.
  * Compile optimized code.
  * Upgrade to debhelper 5.
  * Move maintainance to Debian Games Group.
  * Improve copyright notice in debian/copyright and about box.
  * Clean up build process, significantly reducing dependencies.
  * Move "terminate network game" button behind about button. (Closes: #41998)
  * Updated to policy 3.7.2.  (No changes needed.)

 -- Bas Wijnen <shevek@fmf.nl>  Sun,  7 May 2006 20:22:34 +0200

gfpoken (0.27-2) unstable; urgency=low

  * Let blender write write to a temporary directory. (Closes: #335364)

 -- Bas Wijnen <shevek@fmf.nl>  Thu, 23 Oct 2005 17:51:14 +0200

gfpoken (0.27-1) unstable; urgency=low

  * Added gnome menu entry.
  * Use automake-1.9, not "any".
  * Split graphics from executable.
  * Added real manpage.

 -- Bas Wijnen <shevek@fmf.nl>  Thu, 15 Sep 2005 15:19:34 +0200

gfpoken (0.26-1) unstable; urgency=low

  * Use %zd for printing sizes. (Closes: #323652)
  * Let autoreconf copy files if linking fails. (Closes: #323226)

 -- Bas Wijnen <shevek@fmf.nl>  Thu, 23 Aug 2005 09:31:14 +0200

gfpoken (0.25.dfsg.1-1) unstable; urgency=low

  * New maintainer. (Closes: #219061)
  * Fixed unability to play network games. (Closes: #314543)
  * Updated to debhelper compatibility level 4
  * The Gtk warnings don't seem to be reproducible anymore. (Closes: #60830)
  * Created new artwork in order not to need pov-ray build-dependancy.

 -- Bas Wijnen <shevek@fmf.nl>  Thu, 16 Jun 2005 21:08:04 +0200

gfpoken (0.25-3) unstable; urgency=low

  * Maintainer field set to QA Group
  * Policy updated
  * Removed INSTALL file from documentation
  * Upstream Author(s) -> Author
  * /usr/doc/copyright changed with /usr/share/common-licenses in
    debian/copyright
  * Fixed manpage NAME section

 -- Emanuele Rocca <ema@debian.org>  Thu, 29 Jan 2004 12:04:50 +0100

gfpoken (0.25-2.1) unstable; urgency=low

  * NMU
  * Rebuild to remove dependency on xlib6g.  Closes: #170202.

 -- Daniel Schepler <schepler@debian.org>  Fri, 14 Mar 2003 02:05:13 -0800

gfpoken (0.25-2) unstable; urgency=low

  * Set Build-Depends in debian/control

 -- William Ono <wmono@debian.org>  Sun, 19 Mar 2000 19:02:56 -0800

gfpoken (0.25-1) frozen unstable; urgency=low

  * New upstream version.  The only change is to fix one small but nasty bug:
  * Fixes memory leak in 0.24a (Closes: #50140)

 -- William Ono <wmono@debian.org>  Sat, 15 Jan 2000 17:08:36 -0800

gfpoken (0.24a-4) unstable; urgency=low

  * Fixed reference to /usr/doc in gfpoken.6

 -- William Ono <wmono@debian.org>  Fri, 22 Oct 1999 18:34:25 -0700

gfpoken (0.24a-3) unstable; urgency=low

  * Policy: 3.0.1

 -- William Ono <wmono@debian.org>  Thu, 21 Oct 1999 01:30:51 -0700

gfpoken (0.24a-2) unstable; urgency=low

  * debian/rules now correctly copies iconpix.h to gfpoken.xpm
    (Closes: #43559)

 -- William Ono <wmono@debian.org>  Thu, 26 Aug 1999 13:12:25 -0700

gfpoken (0.24a-1) unstable; urgency=low

  * New upstream version.

 -- William Ono <wmono@debian.org>  Thu, 29 Jul 1999 23:47:29 -0700

gfpoken (0.24-1) unstable; urgency=low

  * New upstream version.

 -- William Ono <wmono@debian.org>  Thu, 29 Jul 1999 22:22:44 -0700

gfpoken (0.23-2) unstable; urgency=low

  * Fixes broken menu entry (Closes: #41816)

 -- William Ono <wmono@debian.org>  Fri, 23 Jul 1999 15:06:13 -0700

gfpoken (0.23-1) unstable; urgency=low

  * New upstream version.
  * Fixes segmentation fault during network game (upstream)
    (Closes: #41688)

 -- William Ono <wmono@debian.org>  Thu, 22 Jul 1999 13:53:09 -0700

gfpoken (0.22-1) unstable; urgency=low

  * New upstream version.

 -- William Ono <wmono@debian.org>  Tue, 13 Jul 1999 20:05:41 -0700

gfpoken (0.21-1) unstable; urgency=low

  * New upstream version.

 -- William Ono <wmono@debian.org>  Tue, 13 Jul 1999 02:17:26 -0700

gfpoken (0.20-1) unstable; urgency=low

  * New upstream version.
  * Can flip between guess and solution at the end of a puzzle (upstream)
    (Closes: #39779)

 -- William Ono <wmono@debian.org>  Mon, 12 Jul 1999 03:24:12 -0700

gfpoken (0.11-1) unstable; urgency=low

  * New upstream version.

 -- William Ono <wmono@debian.org>  Wed,  9 Jun 1999 23:44:05 -0700

gfpoken (0.10-1) unstable; urgency=low

  * Initial Release.

 -- William Ono <wmono@debian.org>  Tue,  1 Jun 1999 22:49:41 -0700

Local variables:
mode: debian-changelog
End:
